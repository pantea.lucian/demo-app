FROM golang:1.13.1 as build-env

WORKDIR /app

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o demo-app .

FROM scratch
COPY --from=build-env /app/demo-app /go/bin/demo-app
COPY --from=build-env /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
EXPOSE 8080
ENTRYPOINT ["/go/bin/demo-app"]