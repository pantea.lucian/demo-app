module gitlab.com/rocore/demo-app

go 1.13

require (
	cloud.google.com/go v0.47.0 // indirect
	cloud.google.com/go/pubsub v1.0.1
	cloud.google.com/go/storage v1.0.0
	github.com/jstemmer/go-junit-report v0.9.1 // indirect
	github.com/stretchr/testify v1.4.0
	golang.org/x/tools v0.0.0-20191012152004-8de300cfc20a // indirect
)
