package apitype

// InboundRequest is the request body for the message handler.
type InboundRequest struct {
	Message string `json:"message"`
}

// NewMessageAlert is the message sent out to notify consumers
// there is a new message.
type NewMessageAlert struct {
	// The name of the object where the new message is stored.
	Name string `json:"name"`
}
