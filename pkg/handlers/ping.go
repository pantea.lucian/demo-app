package handlers

import (
	"net/http"
)

// PingHandler is a handler used for K8s health checks.
func PingHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.Write([]byte("ok"))
}
