package handlers

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPingHandler(t *testing.T) {
	t.Run("Returns response 'ok' with status 200 when /ping called with GET", func(t *testing.T) {
		req, err := http.NewRequest("GET", "/ping", nil)
		assert.NoError(t, err)

		response := httptest.NewRecorder()
		handler := http.HandlerFunc(PingHandler)
		handler.ServeHTTP(response, req)

		// Validate endpoint returns 200
		assert.Equal(t, response.Code, http.StatusOK)

		// Validate response body "ok"
		assert.Equal(t, response.Body.String(), "ok")
	})
}
